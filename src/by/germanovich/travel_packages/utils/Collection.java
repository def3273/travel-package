package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.components.tour.Tour;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Collection {

    private List<Agency> agency = new ArrayList<>();
    private List<Customer> customer = new ArrayList<>();

    private volatile static Collection uniqueInstance;

    private Collection() {
    }

    public static Collection getInstance(){
        if(uniqueInstance == null){
            synchronized (Collection.class){
                if(uniqueInstance == null){
                    uniqueInstance = new Collection();
                }
            }
        }
        return uniqueInstance;
    }

    public List<Agency> getAgency() {
        return agency;
    }

    public void setListAgency(List<Agency> agency) {
        this.agency.addAll(agency);
    }

    public void setAgency(Agency agency) {
        this.agency.add(agency);
    }

    public List<Customer> getCustomer() {
        return customer;
    }

    public void setListCustomer(List<Customer> customer) {
        this.customer.addAll(customer);
    }

    public void setCustomer(Customer customer) {
        this.customer.add(customer);
    }

    public void setTour(Tour tour, int numberAgency) {
        Agency agency = this.agency.get(numberAgency);
        agency.addTour(tour);
    }

    public void setOrder(Order order, int numberAgency) {
        Agency agency = this.agency.get(numberAgency);
        agency.addOrder(order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collection that = (Collection) o;
        return Objects.equals(agency, that.agency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agency);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Collection.class.getSimpleName() + "[", "]")
                .add("agency=" + agency)
                .add("customer=" + customer)
                .toString();
    }
}
