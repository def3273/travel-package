package by.germanovich.travel_packages.components.customer;

import java.util.StringJoiner;

public abstract class Customer {

    private static int idGenerator = 1;
    private int id;
    private String name;
    private String surname;

    public Customer(String name, String surname) {
        setName(name);
        setSurname(surname);
        this.id = getId();
    }

    public Customer() {
        this.id = getId();
    }

    public int getId() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public abstract int getCardNumber();

    @Override
    public String toString() {
        return new StringJoiner(", ", Customer.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .toString();
    }
}
