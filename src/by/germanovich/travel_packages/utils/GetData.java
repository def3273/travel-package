package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import java.util.Arrays;

public class GetData {

    public static int addNumberAgency(Collection myCollection) {

        int numberAgency;

        do {
            System.out.println("Выберите агенство: ");
            PrintData.printAgency(myCollection);
            numberAgency = Keyboard.inputNumber() - 1;

            if (numberAgency >= myCollection.getAgency().size()) {
                System.out.println("Агенства под таким числом нет");
            }

        } while (numberAgency >= myCollection.getAgency().size());

        return numberAgency;
    }

    public static int addNumberCustomer(Collection myCollection) {

        int numberCustomer;

        do {
            System.out.println("Выберите пользователя:");
            PrintData.printCustomer(myCollection);
            numberCustomer = Keyboard.inputNumber() - 1;

            if (numberCustomer >= myCollection.getCustomer().size()) {
                System.out.println("Пользователя под таким числом нет");
            }

        } while (numberCustomer >= myCollection.getCustomer().size());

        return numberCustomer;
    }

    public static int addNumberTour(Collection myCollection, int numberAgency) {
        int numberTour;
        do {
            System.out.println("Выберите тур:");
            PrintData.printTour(myCollection, numberAgency);
            numberTour = Keyboard.inputNumber() - 1;

            if (numberTour >= myCollection.getAgency().get(numberAgency).getTour().size()) {
                System.out.println("Тура под таким числом нет");
            }

        } while (numberTour >= myCollection.getAgency().get(numberAgency).getTour().size());

        return numberTour;
    }

    public static Purpose addPurpose() {
        Purpose purpose;

        System.out.println("Введите цель тура: ");
        System.out.println(Arrays.toString(Purpose.values()));
        System.out.print("Введите цифру(1-" + Purpose.values().length + "): ");

        switch (Keyboard.inputNumber()) {
            case 1:
                purpose = Purpose.RECREATION;
                break;
            case 2:
                purpose = Purpose.EXCURSION;
                break;
            case 3:
                purpose = Purpose.TREATMENT;
                break;
            case 4:
                purpose = Purpose.SHOPPING;
                break;
            default:
                System.out.println("Цель тура - SHOPPING");
                purpose = Purpose.EXCURSION;
        }
        return purpose;
    }

    public static Food addFood() {
        Food food;

        System.out.println("Введите тип питания: ");
        System.out.println(Arrays.toString(Food.values()));
        System.out.print("Введите цифру(1-" + Food.values().length + "): ");

        switch (Keyboard.inputNumber()) {
            case 1:
                food = Food.BREAKFAST;
                break;
            case 2:
                food = Food.DINNER;
                break;
            case 3:
                food = Food.BRUNCH;
                break;
            case 4:
                food = Food.ALL_INCLUSIVE;
                break;
            default:
                System.out.println("Тип питания - BREAKFAST");
                food = Food.BREAKFAST;
        }
        return food;
    }

    public static Transport addTransport() {
        Transport transport;

        System.out.println("Введите тип транспорта: ");
        System.out.println(Arrays.toString(Transport.values()));
        System.out.print("Введите цифру(1-" + Transport.values().length + "): ");

        switch (Keyboard.inputNumber()) {
            case 1:
                transport = Transport.CAR;
                break;
            case 2:
                transport = Transport.BUS;
                break;
            case 3:
                transport = Transport.PLANE;
                break;
            default:
                System.out.println("Тип транспорта - BUS");
                transport = Transport.BUS;
        }
        return transport;
    }

    public static String addNameCountry() {
        System.out.print("Введите страну: ");
        return Keyboard.getName();
    }

    public static String addNameTour() {
        System.out.println("Введите название тура: ");
        return Keyboard.getName();
    }

    public static int addDuration() {
        System.out.print("Введите кол-во дней пребывания: ");
        return Keyboard.inputNumber();
    }

    public static int addPrice() {
        System.out.print("Введите стоимость тура: ");
        return Keyboard.inputNumber();
    }

    public static int addCardNumber() {
        System.out.print("Введите номер карты: ");
        return Keyboard.inputNumber();
    }

    public static String addLocation() {
        System.out.print("Введите место нахождение: ");
        return Keyboard.getName();
    }

    public static String addSiteName() {
        System.out.print("Введите название сайта: ");
        return Keyboard.getName();
    }

    public static int addDiscount() {
        System.out.print("Введите скидку на тур(%): ");
        return Keyboard.inputNumberWithZero();
    }

    public static String addNameHotel() {
        System.out.print("Введите название отеля: ");
        return Keyboard.getName();
    }

    public static int addCostHotel() {
        System.out.print("Введите цену отеля: ");
        return Keyboard.inputNumber();
    }
}
