package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.menu.data.DefaultDate;

import java.util.List;

public class MenuOperations {
    public static Collection addTestDate(Collection myCollection) {

        List<Agency> files = DefaultDate.giveDefaultDate();
        myCollection.setListAgency(files);
        return myCollection;
    }

    public static Collection addTestCustomers(Collection myCollection) {
        List<Customer> files = DefaultDate.giveDefaultCustomers();
        myCollection.setListCustomer(files);
        return myCollection;
    }
}
