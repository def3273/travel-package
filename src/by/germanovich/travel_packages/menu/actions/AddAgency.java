package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.utils.Keyboard;

public class AddAgency {

    private static String name;

    public AddAgency() {

        System.out.println("Введите название агенства: ");
        name = Keyboard.getName();
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        AddAgency.name = name;
    }
}
