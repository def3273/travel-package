package by.germanovich.travel_packages.components.customer;

import java.util.StringJoiner;

public class ResidentialCustomer extends Customer{

    private int cardNumber;

    public ResidentialCustomer(String name, String surname, int cardNumber) {
        super(name, surname);
        this.cardNumber = cardNumber;
    }

    public ResidentialCustomer() {
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ResidentialCustomer.class.getSimpleName() + "[", "]")
                .add(super.getName() + " " + super.getSurname())
                .add("cardNumber=" + cardNumber)
                .toString();
    }
}
