package by.germanovich.travel_packages.components.tour;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;
import by.germanovich.travel_packages.utils.FinanceCalculation;

import java.util.StringJoiner;

public class EconomyTour extends Tour implements FinanceCalculation {

    private int discount;

    public EconomyTour(String country, String name, Purpose purpose, Transport transport, Food food, int duration,
                       int price, int discount) {

        super(country, name, purpose, transport, food, duration, price);
        this.discount = discount;
    }

    public EconomyTour() {
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public int tripPrice() {
        double price = (double) getPrice();
        if(getDiscount() < 1){
            return getPrice();
        }
        return (int)(price / 100 * getDiscount());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EconomyTour.class.getSimpleName() + "[", "]")
                .add("price=" + super.getPrice() + "$")
                .add("discount=" + discount + "$")
                .toString();
    }
}
