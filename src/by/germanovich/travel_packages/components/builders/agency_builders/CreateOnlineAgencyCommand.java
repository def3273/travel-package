package by.germanovich.travel_packages.components.builders.agency_builders;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.agency.OnlineAgency;
import by.germanovich.travel_packages.components.builders.CommandParams;

public class CreateOnlineAgencyCommand extends AbstractCreateAgencyCommand {
    @Override
    protected OnlineAgency createNewCustomer(CommandParams params){
        return agencyFactory.createOnlineAgency();
    }

    protected void fillParams(Agency agency, CommandParams params){
        params.addSiteName();
        OnlineAgency onlineAgency = ((OnlineAgency)agency);
        onlineAgency.setSiteName(params.getSiteName());
    }
}
