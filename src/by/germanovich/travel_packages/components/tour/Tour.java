package by.germanovich.travel_packages.components.tour;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import java.util.Objects;
import java.util.StringJoiner;


public abstract class Tour implements Comparable<Tour> {

    private static int idGenerator = 1;
    private int id;
    private String country;
    private String name;
    private Purpose purpose;
    private Transport transport;
    private Food food;
    private int duration;
    private int price;

    public Tour(String country, String name, Purpose purpose, Transport transport, Food food, int duration, int price) {

        setCountry(country);
        setName(name);
        setPurpose(purpose);
        setTransport(transport);
        setFood(food);
        setDuration(duration);
        setPrice(price);
        this.id = getId();
    }

    public Tour() {

        this.id = getId();
    }

    public int getId() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public Food getFood() {
        return food;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tour tour = (Tour) o;
        return duration == tour.duration &&
                price == tour.price &&
                Objects.equals(country, tour.country) &&
                Objects.equals(name, tour.name) &&
                purpose == tour.purpose &&
                transport == tour.transport &&
                food == tour.food;
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, name, purpose, transport, food, duration, price);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Tour.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("country='" + country + "'")
                .add("name='" + name + "'")
                .add("purpose=" + purpose)
                .add("transport=" + transport)
                .add("food=" + food)
                .add("duration=" + duration)
                .add("price=" + price)
                .toString();
    }

    @Override
    public int compareTo(Tour anotherTour) {
        return this.price - anotherTour.getPrice();
    }
}
