package by.germanovich.travel_packages.components.builders;

import by.germanovich.travel_packages.utils.Collection;

public interface Command {

    void execute(CommandParams params, Collection collectionData);
}
