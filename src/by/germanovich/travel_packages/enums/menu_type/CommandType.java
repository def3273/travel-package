package by.germanovich.travel_packages.enums.menu_type;

public enum CommandType {
    CREATE_RESIDENTIAL_CUSTOMER,
    CREATE_BUSINESS_CUSTOMER,
    CREATE_ONLINE_AGENCY,
    CREATE_LOCAL_AGENCY,
    CREATE_ECONOMY_TOUR,
    CREATE_ELITE_TOUR,
    CREATE_ORDER;
}
