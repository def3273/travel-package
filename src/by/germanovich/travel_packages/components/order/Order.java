package by.germanovich.travel_packages.components.order;

import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.tour.Tour;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.StringJoiner;

public class Order implements Comparable<Order> {

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy MMMM dd HH:mm:ss");

    private static int idGenerator = 1;
    private int id;
    private Customer customer;
    private Tour tour;
    private GregorianCalendar dateCalendar;

    public Order(Customer customer, Tour tour) {
        setCustomer(customer);
        setTour(tour);
        setDateCalendar();
        this.id = getId();
    }

    public Order() {
        setDateCalendar();
        this.id = getId();
    }

    public int getId() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Tour getTour() {
        return tour;
    }

    public GregorianCalendar getDate() {
        return dateCalendar;
    }

    public void setDateCalendar(GregorianCalendar dateCalendar) {
        this.dateCalendar = dateCalendar;
    }

    public void setDateCalendar() {
        dateCalendar = new GregorianCalendar();
    }

    @Override
    public int compareTo(Order order) {
        {
            if (this.id == order.id) {
                return 0;
            } else if (this.id < order.id) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("customer=" + customer)
                .add("tour=" + tour)
                .add("dateCalendar=" + DATE_FORMAT.format(dateCalendar.getTime()))
                .toString();
    }
}
