package by.germanovich.travel_packages.components.agency;

import java.util.StringJoiner;

public class LocalAgency extends Agency {

    private String location;

    public LocalAgency(String name, String location) {

        super(name);
        this.location = location;
    }
    public LocalAgency() {
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LocalAgency.class.getSimpleName() + "[", "]")
                .add("location='" + location + "'")
                .toString();
    }
}
