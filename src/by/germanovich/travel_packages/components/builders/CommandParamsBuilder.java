package by.germanovich.travel_packages.components.builders;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

public class CommandParamsBuilder {

    private CommandParams commandParams;

    public CommandParamsBuilder() {
        this.commandParams = new CommandParams();
    }

    public CommandParamsBuilder addName(String name) {
        this.commandParams.setName(name);
        return this;
    }


    public CommandParamsBuilder addSurname(String surname) {
        this.commandParams.setSurname(surname);
        return this;
    }

    public CommandParamsBuilder addDuration(int duration) {
        this.commandParams.setDuration(duration);
        return this;
    }

    public CommandParamsBuilder addPrice(int price) {
        this.commandParams.setPrice(price);
        return this;
    }

    public CommandParamsBuilder addTransport(Transport transport) {
        this.commandParams.setTransport(transport);
        return this;
    }

    public CommandParamsBuilder addFood(Food food) {
        this.commandParams.setFood(food);
        return this;
    }

    public CommandParamsBuilder addCountry(String country) {
        this.commandParams.setCountry(country);
        return this;
    }

    public CommandParamsBuilder addPurpose(Purpose purpose) {
        this.commandParams.setPurpose(purpose);
        return this;
    }

    public CommandParams build() {
        return this.commandParams;
    }

}
